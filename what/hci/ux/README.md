---
Title: What is User Experience / UX?
Subtitle: How You Experience Something You Use
Query: true
---

*User experience* (UX) is how a [user](/what/users/) feels about something. The [user interface](../ui) is a big part of that, but not the only part. Thinking about how an *actual user* will use what you are making is critical to creating something of value. This is why those who "scratch their own itch" tend to make better software and systems.
