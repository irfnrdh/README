---
Title: A README File
Query: true
---

A README file is a long-standing tradition in the tech world for including a file that contains the most critical information for getting up and running with a thing. These days the exact file name is almost always `README.md` since [Markdown](/lang/md/) has dominated the world of [knowledge source](/what/knowledge/).
