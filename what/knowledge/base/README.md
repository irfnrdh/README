---
Title: What is a Knowledge Base?
Subtitle: Better Than Books 
Query: true
---

A *knowledge base* is simply a collection of structured and unstructured information in a way that can be consumed, shared, and queried. This [knowledge app](/what/knowledge/apps/) is built on a [rotating release](/what/rotating/) knowledge base.
