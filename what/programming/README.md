---
Title: What is Computer Programming?
Subtitle: Set of Instructions Given to a Computer
Query: true
---

To *program* means to give instructions to a [computer](https://duck.com/lite?kae=t&q=computer) usually by writing [computer language code](/lang/). When used as a noun, a *program* is a completed set of such instructions ready to pass to the computer to be done.

*Programming* is simply the process of writing programs as a *programmer*.

:::co-faq

## What is the difference between a *coder* and a *programmer* and a *software engineer*?

Not much, the terms are used rather interchangeably. If there is any difference at all it is that a programmer is a coder but a coder isn't necessarily a programmer. For example, someone who writes nothing but HTML and CSS might call themselves a coder more than a programmer (although that is fine as well). Someone who just writes JSON code (which is definitely not a programming language) might still call themselves a coder. 

Unlike a coder, a programmer usually understands multiple programming [languages](/lang/) and [computer programming paradigms](https://duck.com/lite?kae=t&q=computer programming paradigms) but might not be involved in the higher-level [IT architecture](https://duck.com/lite?q=IT architecture) of a large [system](https://duck.com/lite?q=system) or [software application](https://duck.com/lite?q=software application) like a [software engineer](https://duck.com/lite?q=software engineer) would be. It is common for contract programmers to be hired to program specific things they are given by the developers and architects.

:::
