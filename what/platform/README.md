---
Title: What is a Platform?
Subtitle: A Computing Environment Where Programs Run
Query: true
---

In tech speak, a *platform* is a [computing environment](https://duck.com/lite?kae=t&q=computing environment) in which [software applications](https://duck.com/lite?q=software applications) are installed and [executed](/what/executed/).
