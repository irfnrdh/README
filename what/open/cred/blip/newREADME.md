---
Title:   'Ground Level Technical Proficiency'
Name:    'GLTP'
UUID:    'rwx.gg/gltp'
Version: 'v0.0.1'

Description: |
  Ground Level Technical Proficiency outlines dependent skills, knowledge, and abilities required before pursuing further learning to master of any of the following high-growth technical occupations. Any requirement being considered must prove necessary for *all* occupations in the list.

  * Offensive Security Pentester
  * Security Forensics Investigator
  * Full-Stack Web Developer 
  * Senior Software Developer
  * Site Reliability Engineer
  * Machine Learning Engineer
  * IoT Embedded Device Developer
  * Data Scientist

Requirements:
- R: Become a Trustworthy Member of Human Society
  - R: Think Globally
    - R: What if Everybody Did it? :)
    - R: What Can I Get Away With? :(
    - R: Build and Seek Trust
  - R: Develop Emotional Intelligence 
    - R: Know and Manage One's Emotions
      - R: Controlling Rage
      - R: Sooth Anxiety
      - R: Don't Worry
      - R: Be Happy
      - R: Remain Patient
      - R: Control Impluses
      - R: Manage Melancholy
      - R: Self-Motivate
    - R: Recognizing Emotions in Others
      - R: Cultivate Empathy
    - R: Handle Relationships
      - R: Dude, Be Nice
      - R: Respect Muggles, Don't Make Anyone Feel Small
      - R: Share Knowledge Responsibly
        - R: Share, Don't Hoard
        - R: Be Equally Open About Success and Failure
        - R: Have Strong, Well-Researched Opinions
        - R: Hold Opinions Loosely, Be Open to Change
        - R: Explain Opinions, Don't Start with Conclusions
        - R: Accept and Give Useful Criticism 
        - R: Smile, Lauch, Don't Take Yourself Too Seriously
---

1. Perform Basic Academic Skills
    1. Communicate Effectively and Efficiently
        1. Read (English)
        1. Write (English)
        1. Type 30+ WPM
    1. Do Basic Math
        1. Use Operators
            1. Addition
            1. Subtraction
            1. Multiplication
            1. Division
            1. Modulo
        1. Use Variables
        1. Use Functions
        1. Convert Between Bases
            1. Base2 (Binary)
            1. Base10 (Decimal)
            1. Base16 (Hexadecimal)
            1. Base64
    1. Become an Autodidact
        1. Do Your Own Research
        1. Think Critically
        1. Identifying Quality Information
        1. Challenge Yourself
            1. Practice Your Own Exercises
            1. Create Your Own Projects 
            1. Perform Self-Assessment
            1. Learn from Failures

1. Own and Use a Computer
    1. Use Linux (Debian-Based) OS
    1. Use Apple Mac OS
    1. Use Microsoft Windows OS
    1. Use Raspberry Pi

1. Knowledge Content Creation
    1. Simplified Pandoc Markdown
    1. Structured Data
        1. JSON
        1. YAML
        1. TOML (INI)
        1. XML
        1. Delimited
            1. CSV
    1. HTML
    1. CSS
    1. Image Capture and Editing
        1. Screenshots
        1. Raster
        1. Vector

1. Use Basic Tools and Utilities
    1. Use Desktop Environment
    1. Manage Passwords Securely
    1. Use Chromium-Based Web Browser
    1. Expand Compressed Files
    1. Use Terminal Interface 
        1. Use Basic Commands
        1. Use Secure Shell

1. Use Developer Tools 
    1. Edit Text and Code
        1. Use VSCode (GUI)
        1. Use Nano (TUI)
        1. Use Vim (TUI)
        1. Use GitLab
    1. Source Control
        1. Git
    1. Use Virtualisation Software
    1. Use Docker Containerization
    1. Use Chrome/Chromium DevTools
    1. Use Curl
    1. Use OpenPGP (GPG)

1. Use Essential Services
    1. Communications
        1. Email
        1. Chat Services 
        1. Social Media
        1. Streaming
        1. Newsgroups
        1. Mailing Lists
    1. Hosting
        1. Source
            1. GitHub
            1. GitLab
        1. Web
        1. Domain
        1. Platform
            1. Digital Ocean
1. Develop Basic Applications
    1. Core Languages
        1. JavaScript
        1. C Basics
            1. Data Structures
            1. Algorithms
1. Basic Network Setup and Administration 
    1. VPN
        1. OpenVPN
    1. TCP/IP Model
    1. OSI Model
    1. Router Maintenance
1. Understand Basically How Internet Works

## Missions and Goals

Activate those with experience who don't necessarily have *any* academic training.

* Identify them.
* 

## Open Credential Requirements Model

Just a data model for consistently capturing consistently evolving requirements for a given credential.

## Concepts

### The Hacker Mentality

*Hacker* mentality is thinking *outside* the box to get *inside* the box, a state of mind where one is constantly seeking alternative means of accomplishing a task.

"Just cuz it works is not enough, learn *why* it works."

"The amount of learning it directly proportional to the amount of broken stuff around."

### Focusing on Solutions

Learn to solve. Solve to learn.

### Truth Through Questions

Plato thought everyone has the truth in them, we just forgot it (Socractic Method, maieutics).

### The Downsides of Curiosity

"Knowledge is a mile wide and an inch deep"

### How to Motivate?

1. "Fill the bucket"
1. "Light a fire."
1. "Fan a flame." (preferred)

"What do you do if there's no flame?"

"*Everyone* has a fire, you just have to find it."

"The flame is desire itself. If there is no desire there is no desire even to eat."

"People don't spend time trying to figure out their flame."

"Trust."

### How to Set Boundaries without Damaging Trust



# FAQ

## Questions

* What do we use to determine typing speed?
* What are the specifics for all this?

## Typing Resources

* <https://nitrotype.com>
* <https://typing.com>
* <https://zty.pe>
* <http://www.mavisbeaconfree.com/>



Resources:
  Books:
    - Name: The Tao of TMUX
      URL:  https://leanpub.com/the-tao-of-tmux/read 
    - Name: Learn PowerShell in a Month of Lunches
      URL:  <https://www.manning.com/books/learn-windows-powershell-in-a-month-of-lunches-third-edition>
    - Name: Art-Learning-Journey-Optimal-Performance
      URL:  https://www.amazon.com/Art-Learning-Journey-Optimal-Performance/dp/0743277465
    - Name: Drive
      URL

--------------------

Old stuff


---
Title:  'Ground Level Technical Proficiency'
Name:   'GLTP'
UUID:   'rwx.gg/gltp'

Description: |
  The Ground Level Technical Proficiency is an open credential covering skills,
  knowledge, and abilities required before pursuing most occupations involving
  technology. 

  Applications

  * Full-Stack Web Developer 
  * Senior Software Developer
  * Data Scientist
  * IoT Developer

  Operations

  * Site Reliability Engineer
  * Database Administrator
  * Offensive Security Engineer (Pentester)
  * Forensic Investigator

  These lists are not complete, but serve as a guide when deciding if
  a requirement should be included in this foundational credential.

Requirements:
  - R: 'Use Computer Input and Output.'
    - R: 'Read thoroughly and for maximum comprehension'
    - R: 'Scan technical documentation effectively.'
    - R: 'Use a qwerty keyboard and understand its layout.'
    - R: 'Touch type 40 words per minute.'
    - R: 'Use a three button mouse effectively.'
    - R: 'Use standard touch track pad effectively.'

  - R: 'Work with knowledge source.'
    - R: 'Capture knowledge source.'
      - R: 'Keep a log.'
      - R: 'Take good notes.'
      - R: 'Write effectively'
        - R: 'Consider the objective.'
        - R: 'Know your audience.'
        - R: 'Decide what to include.'
        - R: 'Polish presentation.'
      - R: 'Code simplified Pandoc Markdown.'
      - R: 'Screen capture a region.'
    - R: 'Manage knowledge source.'
    - R: 'Search knowledge source.'
    - R: 'Share knowledge source.'
    - R: 'Discover knowledge source.'

  - R: 'Understand computer hardware.'
  - R: 'Code proficiently in multiple languages.'
    - R: 'Code in HTML.'
    - R: 'Code in CSS.'
    - R: 'Code in JavaScript (DOM only).'
    - R: 'Code in Go.'
    - R: 'Code in Python3.'

  - R: 'Understand and use structured data formats.'

  - R: 'Use virtual server software'
    - R: 'Use VMware Workstation'
    - R: 'Use VirtualBox'

  - R: 'Use Linux.'
      - R: 'Describe the history of Linux.'
      - R: 'Install Linux.'
      - R: 'Describe and demonstrate basic shell usage.'
        - R: 'Explain what a REPL is.'
        - R: 'Explain what a command prompt is.'
          - R: 'Identify the username in prompt.'
          - R: 'Identify the hostname in prompt.'
          - R: 'Explain what the ~ (tilde) means.'
        - R: 'Demonstrate use of command history.'
        - R: 'Demonstrate use of several simple commands.'
        - R: 'Demonstrate how to end a terminal session.'
      - R: 'Customize the shell (.bashrc,etc.)'
      - R: 'Navigate the shell.'
        - R: 'Demonstrate and explain the `ls` command.'
        - R: 'Demonstrate and explain the `file` command.'
        - R: 'Demonstrate and explain the `less` command.'
        - R: 'Demonstrate and explain the `pwd` command.'
        - R: 'Demonstrate and explain the `cd` command.'
        - R: 'Demonstrate and explain the `man` command.'
        - R: 'Explain what a directory is.'
        - R: 'Explain the ./ directory.'
        - R: 'Explain the ../ directory.'
        - R: 'Explain / (root) directory.'
        - R: 'Explain absolute and relative paths.'
        - R: 'Explain how file and directory names work.'
          - R: 'Explain file system case sensitivity.'
          - R: 'Explain why and how to avoid spaces in filenames.'
          - R: 'Explain which punctuation is preferred in filenames.'

      - R: 'Demonstrate and explain Secure Shell (SSH).'

      - R: 'Explain and cite the Filesystem Heirarchy Standard (FHS).'

      - R: 'Explain shell "hashing".'

Resources:
  Books:
    - Name: The Tao of TMUX
      URL:  https://leanpub.com/the-tao-of-tmux/read 
    - Name: Learn PowerShell in a Month of Lunches
      URL:  <https://www.manning.com/books/learn-windows-powershell-in-a-month-of-lunches-third-edition>
    - Name: Art-Learning-Journey-Optimal-Performance
      URL:  https://www.amazon.com/Art-Learning-Journey-Optimal-Performance/dp/0743277465
    - Name: Drive
      URL:  ?? 
---

# Ground Zero Certified Technologist (G0CT)

Requirements, description, and resources for obtaining the Ground Zero Certified Technologist open credential.

## Suggested Syllabus

1. Practice Critical Social Skills
    1. Be Nice
    1. Temper Frustration
    1. Remain Patient
    1. Develop Emotional Intelligence
    1. Accept and Give Useful Criticism 
    1. Explain Opinions and Conclusions
    1. Honor Muggles
1. Perform Basic Academic Skills
    1. Communicate Effectively and Efficiently
        1. Read (English)
        1. Write (English)
        1. Type 30+ WPM
    1. Do Basic Math
        1. Use Operators
            1. Addition
            1. Subtraction
            1. Multiplication
            1. Division
            1. Modulo
        1. Use Variables
        1. Use Functions
        1. Convert Between Bases
            1. Base2 (Binary)
            1. Base10 (Decimal)
            1. Base16 (Hexadecimal)
            1. Base64
    1. Become an Autodidact
        1. Do Your Own Research
        1. Think Critically
        1. Identifying Quality Information
        1. Challenge Yourself
            1. Practice Your Own Exercises
            1. Create Your Own Projects 
            1. Perform Self-Assessment
            1. Learn from Failures

1. Own and Use a Computer
    1. Use Linux (Debian-Based) OS
    1. Use Apple Mac OS
    1. Use Microsoft Windows OS
    1. Use Raspberry Pi

1. Knowledge Content Creation
    1. Simplified Pandoc Markdown
    1. Structured Data
        1. JSON
        1. YAML
        1. TOML (INI)
        1. XML
        1. Delimited
            1. CSV
    1. HTML
    1. CSS
    1. Image Capture and Editing
        1. Screenshots
        1. Raster
        1. Vector

1. Use Basic Tools and Utilities
    1. Use Desktop Environment
    1. Manage Passwords Securely
    1. Use Chromium-Based Web Browser
    1. Expand Compressed Files
    1. Use Terminal Interface 
        1. Use Basic Commands
        1. Use Secure Shell

1. Use Developer Tools 
    1. Edit Text and Code
        1. Use VSCode (GUI)
        1. Use Nano (TUI)
        1. Use Vim (TUI)
        1. Use GitLab
    1. Source Control
        1. Git
    1. Use Virtualisation Software
    1. Use Docker Containerization
    1. Use Chrome/Chromium DevTools
    1. Use Curl
    1. Use OpenPGP (GPG)

1. Use Essential Services
    1. Communications
        1. Email
        1. Chat Services 
        1. Social Media
        1. Streaming
        1. Newsgroups
        1. Mailing Lists
    1. Hosting
        1. Source
            1. GitHub
            1. GitLab
        1. Web
        1. Domain
        1. Platform
            1. Digital Ocean
1. Develop Basic Applications
    1. Core Languages
        1. JavaScript
        1. C Basics
            1. Data Structures
            1. Algorithms
1. Basic Network Setup and Administration 
    1. VPN
        1. OpenVPN
    1. TCP/IP Model
    1. OSI Model
    1. Router Maintenance
1. Understand Basically How Internet Works

## Missions and Goals

Activate those with experience who don't necessarily have *any* academic training.

* Identify them.
* 

## Open Credential Requirements Model

Just a data model for consistently capturing consistently evolving requirements for a given credential.

## Concepts

### The Hacker Mentality

*Hacker* mentality is thinking *outside* the box to get *inside* the box, a state of mind where one is constantly seeking alternative means of accomplishing a task.

"Just cuz it works is not enough, learn *why* it works."

"The amount of learning it directly proportional to the amount of broken stuff around."

### Focusing on Solutions

Learn to solve. Solve to learn.

### Truth Through Questions

Plato thought everyone has the truth in them, we just forgot it (Socractic Method, maieutics).

### The Downsides of Curiosity

"Knowledge is a mile wide and an inch deep"

### How to Motivate?

1. "Fill the bucket"
1. "Light a fire."
1. "Fan a flame." (preferred)

"What do you do if there's no flame?"

"*Everyone* has a fire, you just have to find it."

"The flame is desire itself. If there is no desire there is no desire even to eat."

"People don't spend time trying to figure out their flame."

"Trust."

### How to Set Boundaries without Damaging Trust



# FAQ

## Questions

* What do we use to determine typing speed?
* What are the specifics for all this?

## Typing Resources

* <https://nitrotype.com>
* <https://typing.com>
* <https://zty.pe>
* <http://www.mavisbeaconfree.com/>



