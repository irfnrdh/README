---
Title: What is an Open Credential?
Subtitle: Community-Created Checklist of How to Prove Your Skills, Knowledge, and Abilities
Query: true
---

An *open credential* is essentially a crowd-sourced checklist of things a person should do to prove they have attained a certain level of skills mastery and knowledge. No one gives it to you, you give it to yourself when you feel you have earned it. If you don't trust yourself to make that self-assessment you can enlist the help of one or more experienced people to verity the work you have done --- perhaps even some who make it their occupation to perform such validations professionally and whose profession entirely depends on the trustworthy quality of their validations.

## Trust Instrument

The credential is a *trust instrument*. It is something you can use to foster trust with anyone evaluating you be it for a job, school application, or even acceptance into a club or community. Others can validate the credential since it is written, tangible, and verifiable. They can further reach out to those whom you have listed as having validated your work to attain the credential.

## Defeats Corruption

Corruption is killing credentialing systems today. It is no secret that people often pay to beat any system (as has been in the press a lot in 2019).

The flaw in the system is that no one is financially motivated to be trustworthy because every one of these systems answers to the person or organization providing the money. This means such systems often get money for *not* being trustworthy and have not been held accountable. 

Even when they are held accountable it is not to the person who is being assessed, but to some other organization or institution. This is how we end up with "certification" systems that don't even provide a copy of the assessment answers to the person who paid to take the test. This is *completely* upside down and costs millions in overhead because antagonistic test-taking systems pit the person taking the test against the test and the organization that prepared it.

Open credentials defeat corruption by turning the system right-side up again and building on a foundation of trust between the person being evaluated and the one doing the evaluating. This sustainable approach checks itself. If the person being evaluated doesn't believe they are being evaluated correctly they simply leave and find someone they find more capable, more trustworthy. 

The free-market brings the best evaluators to the surface --- even without advertising and marketing --- because of trustworthy word-of-mouth references between those who have successfully been evaluated and those seeking their own evaluations.

If the person being evaluated lies to themself and continues to persist until they find someone willing to rubber-stamp their open credential then *both* the person doing the rubber stamping *and* the person seeking to get away with lying about their skills is punished *by the market* when that person tries to do anything with that credential. 

Ultimately the assessors involved in hiring and admissions provide the final, critical check to the entire system. The final assessment is facilitated by the fact that the open credential contains specific details about the claims to skills and knowledge that can be *individually validated by anyone* evaluating that person. 

An open credential is essentially a list of evidence that can be easily proved or disproved. Today, without open credentials, such proof is prohibitively difficult to obtain forcing everything from hiring on "feel" to subjecting people with social disabilities to unnecessarily intense and irrelevant "white board interviews."

Open credentials promise and promote an ecosystem of skills and knowledge founded on the bedrock principles of the free market: trust, reputation, and mutually beneficial exchange.

