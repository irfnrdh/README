---
Title: What is an Open Course?
Subtitle: One That Allows Anyone to Copy, Change, and Share
Query: true
---

An *open course* is one that allows all the content to be and used however you want --- even sold so long as you provide [full legal attribution](/copyright/) in any derivative work and release your work and changes under the same terms. If it sounds familiar that is because the [Creative Commons](https://duck.com/lite?kae=t&q=Creative Commons) licensing was inspired by the [open source](/what/open/source/) software movement.



