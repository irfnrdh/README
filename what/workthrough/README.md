---
Title: Workthrough
Subtitle: Working Through Something Together
Query: true
---

A *workthrough* (not to be confused with a
[walkthrough](/what/walkthrough/)) is when two or more people work
through a challenge or task together to arrive at a desired result.
