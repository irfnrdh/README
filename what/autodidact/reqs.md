---
Title:   'Autodidact'
Name:    'BLiP'
UUID:    'rwx.gg/autod'
Version: 'v0.0.2'

Description: |

Requirements:
- R: Become an Independent Learner (Autodidact)
  - R: Do Your Own Research
  - R: Think Critically
  - R: Identifying Quality Information
  - R: Challenge Yourself
    - R: Practice Your Own Exercises
    - R: Create Your Own Projects 
    - R: Perform Self-Assessment
    - R: Learn from Failures

- R: Perform Basic Academic Skills
  - R: Communicate
    - R: Read, Write, Speak Native Language Proficiently
    - R: Read, Write, Speak English Proficiently
    - R: Type 30+ WPM
  - R: Perform Basic Technical Math
    - R: Perform Basic Math Operations
      - R: Add
      - R: Subtract
      - R: Multiply
      - R: Divide
      - R: Get Remainder (Modulo)
    - R: Use Variables
    - R: Use Functions
    - R: Count and Convert Between Common Bases
      - R: Convert Base2 (Binary)
      - R: Convert Base10 (Decimal)
      - R: Convert Base16 (Hexadecimal)
      - R: Convert Base64
    - R: Perform Bitwise Operations on Bitfields
      - R: Use NOT
      - R: Use AND
      - R: Use OR
      - R: Use XOR
      - R: Shift Left
      - R: Shift Right
      - R: Rotate

- R: Manage Personal Knowledge and Data
    - R: Write Simplified Pandoc Markdown
    - R: Code Structured Data
        - R: Code JSON
        - R: Code YAML
        - R: Code TOML (INI)
        - R: Code XML
        - R: Code CSV and Delimited
    - R: Code Basic HTML
    - R: Code Basic CSS
    - R: Capture and Edit Simple Images and Diagrams
        - R: Take a Regional Screenshot
        - R: Describe Raster and Vector Formats
        - R: Use Inkscape
        - R: Use Glimpse
        - R: Use Draw.io
        - R: Use Krita

- R: Use Basic Tools and Utilities
    - R: Use Desktop Environment
    - R: Manage Passwords Securely
    - R: Use Chromium-Based Web Browser
    - R: Expand Compressed Files
    - R: Use Terminal Interface 
        - R: Use Basic Commands
        - R: Use Secure Shell

- R: Use Essential Services
    - R: Communications
        - R: Email
        - R: Chat Services 
        - R: Social Media
        - R: Streaming
        - R: Newsgroups
        - R: Mailing Lists
    - R: Hosting
        - R: Source
            - R: GitHub
            - R: GitLab
        - R: Web
        - R: Domain
        - R: Platform
            - R: Digital Ocean

- R: Develop Basic Applications
    - R: Code the Basic 
        - R: Modern JavaScript
        - R: C
        - R: Bash

- R: Basic Network Setup and Administration 
    - R: VPN
        - R: OpenVPN
    - R: TCP/IP Model
    - R: OSI Model
    - R: Router Maintenance

- R: Explain How Internet Works

---

## Learning to Learn, Becoming an Autodidact

An *autodidact* is one who takes learning into their own hands either because they prefer it or are forced to for lack of other resources. An autodidact reads, writes meticulous notes, and executes tests to gain new knowledge and skills. Failure is an integral part of the learning process to an autodidact. They don't fear it, they seek it and are intrigued by it.


