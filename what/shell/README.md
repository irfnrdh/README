---
Title: What is a Terminal Command Line Shell?
Subtitle: The Interpreter That Handles Your Interactive Commands 
Query: true
---

A *shell* is the program waiting for you to typing in commands from a terminal command line. Most all computers have them if you look hard enough. The most important shell program in the world right now is [Bash](/lang/bash/). Others include [Zsh](/advice/dont/zsh/), [`fish` shell](https://duck.com/lite?kae=t&q=fish shell), and [PowerShell](https://duck.com/lite?kae=t&q=PowerShell).

## See Also

* [Bash Scripting Language](/lang/bash/)
