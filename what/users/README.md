---
Title: What are Users?
Subtitle: Everyone Who Uses Something, Like a Computer or a Phone
Query: true
---

You are a *user* when you use things, usually a computer, device, app, or system in the tech context.

:::co-ff
This term entered tech pop culture when [Tron](/reviews/films/tron/) deified users.
:::
