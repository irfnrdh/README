---
Title: What is a Codebook?
Subtitle: A Markdown Notebook in Git with Code
Query: true
---

A *codebook* is simply [Git](/tools/git/) project repository that just contains [Markdown](/lang/md/) files with personal notes and code along with it than can be quickly run from the command line to remind you how something works. Codebooks are particularly valuable because *you* create and maintain them using your own voice and examples.

:::co-btw
Unix and Linux engineers and hackers have been keeping a little black book with notes on how to do specific things since the 70s. It has become something of a cultural icon. These days it is far more efficient to keep those notes in a codebook online that can be accessed from anywhere.
:::
