---
Title: What is DevOps?
Subtitle: Where Development Meets Operations
Query: true
---

Traditionally technology work within an organization is divided into development, the creation of the stuff, and operations, the support of that stuff and the systems and tools people use to make it. DevOps facilitates the connection and relation between the two, both technically and culturally. DevOps engineers are the technologists who support the systems that allow developers to deliver their software as easily as possible and with the highest quality usually through [continuous testing, integration, and deployment](/what/cicd/). These days the term DevOps immediately invokes a few specific technologies: Docker containers, Kubernetes clusters, and CI/CD services like [GitLab](/services/gitlab/).
