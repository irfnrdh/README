---
Title: What are IT Cloud Services?
Subtitle: Paying Someone Else to Take Care of It
Query: true
---

Services are provided over the Web and allow you to do things that once were only possible from rather large IT shops. There are *lots* of things *as-a-service* offerings today. Collectively these are commonly referred to as "cloud" services.

[Software as a Service](./saas/)
[Platform as a Service](./paas/) 
[Infrastructure as a Service](./iaas/) 
[Function as a Service](./faas/)
[Database as a Service](./daas/)

## See Also

* <https://foaas.com>
