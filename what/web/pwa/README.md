---
Title: What are Progressive Web Apps / PWA?
Subtitle: Web Apps That Act Like Native Apps
Query: true
---

A *progressive web app* (PWA) is a web application that behaves like a native mobile or desktop application but without the hassle of any [app store](/advice/dont/appstores/).
