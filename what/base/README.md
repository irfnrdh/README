---
Title: What is a base?
---

A *base* indicates the number of digits in a number system. More common
names for *base* are given to specific bases such as
[binary](https://duck.com/lite?kae=t&q=binary),
[hexadecimal](https://duck.com/lite?kae=t&q=hexadecimal), and
[base64](https://duck.com/lite?kae=t&q=base64). The higher the base the
few the digits needed to communicate the value of a number.

 Decimal    Binary     Octal    Hexadecimal
--------- ----------  -------  -------------
   0         0         0          0
   1         1         1          1
   2        10         2          2
   3        11         3          3
   4       100         4          4 
   5       101         5          5
   6       110         6          6
   7       111         7          7
   8      1000        10          8 
   9      1001        11          9
  10      1010        12          A
  11      1011        13          B
  12      1100        14          C
  13      1101        15          D 
  14      1110        16          E 
  15      1111        17          F 
  16      10000       20         10 

> There are 10 types of people in in the world, those that know binary
and those that don't.

## See Also

* [Binary](https://duck.com/lite?kae=t&q=Binary)
* [Boolean Algebra](https://duck.com/lite?kae=t&q=Boolean Algebra)
* [Turing Machine](https://duck.com/lite?kae=t&q=Turing Machine)
* [PicoCTF 2Warm](/boost/picoctf/2warm/)
