---
Title: What Does Grok Mean?
Subtitle: To Understand Something Well
Query: true
---

*Grok* is a Martian-turned-English word meaning to “get or understand something at a comprehensive level”. Technologists often use this watered down version of the original which first appeared in Robert Heinlein's *Stranger in a Strange Land* to give a word to what Earthlings cannot explain in words other than “to understand so thoroughly that you merge with it and it merges with you.” So saying, “Yeah I *grok* JavaScript” would be immediately suspect to anyone who understands what the term *grok* actually means, but people say it anyway.

## See Also

* [Merriam-Webster's entertaining definition](https://www.merriam-webster.com/dictionary/grok)  

