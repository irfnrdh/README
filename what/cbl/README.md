---
Title: Challenge Based Learning (CBL)
Subtitle: Gamified Instruction at Its Best
Query: true
Created: 2020-06-23T16:37:31-0400
---

Everyone likes a challenge. We learn better when we have to work for
it and have fun in the process. Learning becomes a game pitting us
against our own learning abilities, and sometimes that of others in a
friendly competition. Such gamified challenges have been at the heart
of cybersecurity skills learning since the beginning, but other
learners can benefit as well.

Giving someone a challenge and telling them what to research motivates
and solidifies skills-based learning. Here's how it goes:

M: "Okay, ready for a challenge?"

L: "Yep." 

M: "Why don't you try the `hello` challenge."

L: "Okay, what's that?"

M: "Create a script that outputs `Hello World!` when you type `./hello`
   on the command line. You'll need to research bash, how to create a
   script, and how to print output. I'll check back with you at 4:23,
   which gives you about 10 minutes. I'll give you a *nudge* if you are
   stuck at that time. Do your best to figure it out before then.

L: "Okay, I'm on it!."

It is amazing the amount of motivation in the person's voice is obvious
when presenting learning challenges this way --- especially since so
many are used to searching for answers to things all ready. The few that
aren't need to learn *that* skill --- how to do one's own research ---
even more than the what the challenge skills is about.

## Guided *Research* Approach

Rather than revealing what to do next the guided research approach keeps
the focus on learning with the person doing the learning. 

M: "Have you searched for ...? What did you find out? Where did you
   look? Consider trying this and such web site? Do you have that book
   we discussed available?"

This approach focuses on the concepts and terms because they are crucial
as search parameters. You can't master the skills without knowing what
the vocabulary related to those skills. Again, the primary focus is on
helping the learner to learn, not instructing them at all, but mentoring
them through their own learning process.

## Specification Exercise

Content developed to assist with challenged based instruction is akin to
writing the specification for a job or project. The scope and scale are
usually the only thing different.

Such content has *no specific instruction* in it. That would spoil the
research and learning opportunity. 

Instead, specification exercises use very language that would be easy
find when doing Internet research. The more detail the better, but some
of that detail can be further hidden behind hints and suggestions rather
than specific answers and details about *how* to fulfill the challenge.

:::co-pwz

Remember, it is up to the person learning to write their *own* notes,
which is better than any text book later because they will remember it
better.

:::

Spec exercises also double as fun competitions when done as a group and
timed.

## Walkthrough Solutions

There are usually several ways to fulfill the requirements of a
specification exercise. Those learning are never punished for creating a
solution different than another. Instead the mentor discusses
alternatives and when another approach might be better and why. Learners
can then comment out their other work and add more in other versions as
they understand and improve their skill --- just like the real world.

This mentoring can be supplemented or even replaced with a guided
walkthrough that completely explains and spoils the challenge gradually.
It is *critical* that the learner be able to stop and not spoil their
own learning as they proceed.

## Test-Driven Development

Challenge based learning is effectively [test driven
development](https://duck.com/lite?kae=t&q=test driven development) but
for learning.

## Content Creation

There are three main types of content required:

1. Detailed specification of challenge task
1. Non-specific content related to dependencies
1. Gradually revealed walkthrough solution

For example, the `hello` challenge would have a specification and a
walkthrough but might also depend on a number of other resources
explaining things like the following:

* Editing a File
* Setting Permissions on a File
* Printing Output to the Command Line
* Executing a Script

Ideally you will have your own knowledge base documenting all the
dependent skills and concepts, but allowing learners to simply use the
Internet is also fine provided you give good mentoring on identifying
the best resources for such information.

## Review

* CBL is fun
* CBL emphasizes skill of learning itself
* CBL keeps responsibility with learner 
* CBL requires a specified challenge task
* CBL forces practice with skill
* CBL emphasizes terminology and concepts
* CBL uses gradually revealed walkthroughs

