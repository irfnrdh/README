---
Title: What is a Computer?
Query: true
---

You probably already think you know what a *computer* is. You're using one right now. But did you know that a Computer was once the title of humans whose job it was to compute stuff like tables of numbers for science, economics and more? Once upon a time such tasks took a tremendous amount of time and were very prone to errors which is why [Michael Babbage](https://duck.com/lite?kae=t&q=Michael Babbage) created the first mechanical computer called [the difference engine](https://duck.com/lite?kae=t&q=the difference engine). A woman who took great interest in his creation, [Ada Lovelace](https://duck.com/lite?kae=t&q=Ada Lovelace) is widely acknowledged as the first [programmer](https://duck.com/lite?kae=t&q=programmer).

## See Also

* [Crash Course Computer Science](https://www.youtube.com/playlist?list=PL8dPuuaLjXtNlUrzyH5r6jN9ulIgZBpdo)



