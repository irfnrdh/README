---
Title: Index of Beginner Boosts
Subtitle: One Path to Getting Started in Tech
---

Here's a general outline of what you will need to learn to get a solid
start in a life-long tech career in a way that will make you very
well-rounded.

* Learn How to Learn
* Learn to Use a Computer
* Learn to Use the Internet
* Learn to Use the Linux Terminal
* Learn to Create Content
  * Learn to Code Content
    * Learn to Write JSON
    * Learn to Write YAML
    * Learn to Write XML
    * Learn to Code HTML
    * Learn to Code CSS
  * Learn to Create Images
    * Learn to Create Raster Images
    * Learn to Create Vector Graphics
  * Learn Basic UX Design
