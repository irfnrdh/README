---
Title: Linux Terminal Mastery
Subtitle: Learn to Live on the Command Line
Query: true
---

1. Introduction
    1. What's Linux?
        1. Most Successful Unix Ancestor
        1. Inspired by Minix
        1. Bash and GCC Ported from GNU
        1. Released August 25, 1991
    1. Why Linux?
        1. Ubiquitous
        1. Free and Open Source
        1. Spectacularly Powerful
        1. Essential Tool of RWX Autodidacts
    1. What's So Amazing About the Terminal?
        1. Fastest Human-Computer Interface
        1. Powerful, Even Dangerous
        1. Consistent
        1. Ubiquitous
        1. Remotely Accessible
    1. What Does "Master" Even Mean?
        1. Prefer Living in the Terminal
            1. Vim: Writing and Editing
            1. TMUX: Terminal Management
            1. Script in Bash
            1. Search with Lynx
            1. Email with Mutt
        1. Healthy Disdain for Graphic Interfaces
            1. Unnecessary
            1. Buggy
            1. Violate Privacy
            1. Slow User Interface
            1. Slow Applications
        1. Prone to Program
            1. The Command Line *is* Programming
        1. Cares A Lot About
            1. User Freedoms
            1. Right to Privacy and IP
            1. Free and Sustainable Knowledge
            1. Consistency Over Artistic Expression
            1. Helping and Understanding Others
            1. Continual Learning
            1. Fixing Things
        1. Cares Not About
            1. Windows Managers
            1. Desktop Environments
            1. Linux Distributions
            1. Operating Systems
            1. Distracting Aesthetics
            1. Uninformed Opinions
                1. Feared by Muggles
    1. FAQ
        1. Why Not Unix?
            1. Not Free and Open
            1. Hard to Find
            1. Dying or Dead
        1. Why Not FreeBSD?
            1. Released November 1, 1993
            1. Not Particularly User Friendly
            1. Although Has Other Advantages
                1. More Permissive License
                1. Generally Safer
1. Get Linux
    1. As Software
        1. Download Distro Disk Image File
    1. In Cloud 
        1. Digital Ocean
            1. Rock Solid UX
            1. Regularly Shuts Down Compromised Droplets
            1. Extremely Well Regarded
            1. Promotes Knowledge Sharing
            1. Upload VM Images
            1. Free Tier
        1. Amazon
            1. Biggest in the World
            1. Complicated but Powerful Interface
            1. Elastic Pricing
            1. Free Tier

                
    1. On Hardware
        1. Raspberry Pi
        1. Consider Discarded Hardware
        1. Flash USB Install Drive with Etcher 
            1. FAQ
                1. Why Not `dd`?
                1. Why Not Rufus?
                1. Should I Dual Boot?
    1. FAQ
        1. What About Google Cloud?
          1. Google Regularly Violates Basic Ethics
        1. What About Linode?
          1. Created By a Full-Sail Graduate
          1. *Serious* Security Breech in 2013
          1. Regularly Down from DDoS Attacks
          1. No Automatic Shutdown When Hacked
        1. Isn't a Bash Terminal Enough?
            1. Especially Not Mac
