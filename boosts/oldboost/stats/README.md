---
Title: Stats and Numbers
---

* 16 weeks
* 5 sessions/week
* 3 hours/session
* 15 total session hours/week
* 25 extra focus hours/week
* 40 total hours/week
* 40 minutes/segment
* 3 segments/session
* 80 total sessions
* 32 weekend days
* 112 total days
* 240 total segments 
* 3000+ pages of reading
* 640+ total hours
