
:::todo

-----------------------------------------------------------------------
Learning Project Ideas
-----------------------------------------------------------------------
Write a Hello World program in every course language and others.

Copy your Hello World programs to your notes inside of code fences.

Research the pros and cons of Brave vs Chromium vs Chrome.

Read all the ProtonMail support pages.

Customize your ProtonMail web interface.

Go for a walk and ponder what you *like* to do and write it down.

Ponder what people tell you that you are good at and write it down.

Imagine what your best work day would be like and write it down.

Consider if and how technologies covered match your personal goals.

Post a few messages to the community Discord channel.
-----------------------------------------------------------------------

:::

:::todo

-----------------------------------------------------------------------
Learning Project Ideas
-----------------------------------------------------------------------
Start your notes project repo on GitLab.

Create a learning plan with a daily schedule and your goals.

Port your notes from Day 1 into Basic Pandoc Markdown in notes repo.

Use one of everything from Basic Pandoc Markdown in your notes.

Do your own research to compare Pandoc to other tools and formats.

Take some relevant notes from Google docs and add them to notes repo.

Research different services out there to see which use Markdown.

Complete all of Typing.com.

Invite people in Discord to race you on NitroType.

Play some of the listed typing games.

Force yourself to use one-handed mouse cut and pasting.
-----------------------------------------------------------------------

:::

### Editing Files from Terminal

* Create a Hello World Script in Bash

### Set Up Command-Line Git and GitLab

* Grok Basic Public Key Cryptography
* Generate Secure Shell Keys with [`ssh-keygen`](/tools/ssh/tasks/keygen/)
* Add Public Key to GitLab Account Settings
* Install and Configure Git
* Grok Basic Git Source Code Management
* Configure Local Git User Settings
* Turn an Existing Directory into New GitLab Project Repo
* Clone an Existing GitLab Project Repo

:::todo

-----------------------------------------------------------------------
Learning Project Ideas
-----------------------------------------------------------------------
Write your thoughts about the terminal in your notes.

Identify your terminal limiters and write them in your notes.

Argue with yourself about when [GUI](/what/hci/ui/graphic/) or [TUI](https://duck.com/lite?kae=t&q=TUI) is better.

Practice editing files in Nano for a while to compare.

Practice editing files in Emacs for a while to compare.

Complete all seven parts of `vimtutor`.

Read and practice the [Vi Magic Wands](/tools/editors/vi/how/magic).

Do some of the other [Vim Resources](/tools/editors/vi/#see-also).

Explain what public key cryptography to yourself in your notes
like you are explaining it to someone.

Move your SSH keys and make some more. Repeat until it comes natural.

Research what Git is about and what files it creates on your
computer.

Delete and re-clone a GitLab project a few times to get used to it.

Create several GitLab repos from scratch to memorize the
`git` commands.

Read up on Bash and create a Magic Eight Ball shell script.

Read the first Part of [The Linux Command Line](/reviews/books/tlcl/) in advance.

Navigate around your Linux system with the [survival commands](/lang/bash/#survive).

Teach yourself the Ed editor as well as Vim.
-----------------------------------------------------------------------

:::

## Day 4

[📺]{.noinvert} [Linux Beginner Boost - Day 4 (May 7, 2020)](https://youtu.be/xuBiLyCcTzM)

### Configure Vim

* Configure Local Vim User Settings
* Steal Vim Config from Others
* Review What Vim Config Does
* Avoid [Vimisms](/tools/editors/vim/vimisms/) at All Costs
* Grok Why Small `vimrc` is Better

### Set Up Dotfiles Repo

* Grok Dotfiles Tradition
* Grok Repos Directory Organization
* Set Up Dotfiles GitLab Project Repo

### Configure Bash

* Grok Interactive Command Shells
* Grok Why Bash and *Not* Zsh
* Grok Shell Login Scripts
* Grok Symbolic and Hard Links
* Link `~/.bash_aliases` to Bash Config
* Set Up Bash Environment, Aliases, and Functions
* Steal Bash Config from Others
* Review What Bash Config Does
* Set Up Personal Scripts Directory 

### Get Stuff from Web without Browser

* Curl Stuff from the Web
* Curl `ix` Command from Chat

### Configure TMUX

* Grok Terminal Multiplexors
* Set Up and Use TMUX (Like Screen)
* Steal Others TMUX Config
* Review What TMUX Config Does

### No One is Ever Finished Configuring

:::todo

-----------------------------------------------------------------------
Learning Project Ideas
-----------------------------------------------------------------------
Familiarize yourself with `:help` from within Vim.

Snoop through several other dotfiles directories on GitLab and GitHub.

Learn about `Plug` and add your first plugin.

Customize *all* your configurations by stealing from others.

Look for cool stuff to use `curl` with on the Web.

Practice sharing files and code with friends using `ix`.

Read through all of the TMUX configuration and study about it.

Familiarize yourself with `:help` from within Vim.
-----------------------------------------------------------------------

:::

## Day 5

[📺]{.noinvert} [Linux Beginner Boost - Day 5 (May 8, 2020)](https://youtu.be/k4aSa4EpzkI)

### Basic Networking Overview

* Grok How the Internet Works
* Grok How Your Home Network Works
* Scan Your Network and the Internet

### Remote Secure Shell Connections

* Grok Secure Shell
* Review Secure Shell Configuration
* Practice SSH Connection
* Practice SCP File Transfer

### Configure Lynx 

* Grok Terminal Web Browsers
* Set Up and Use Lynx Web Browser
* Steal Lynx Config from Others
* Set Up Command Line Web Searches

### Personal Privacy and Online Safety

* Danger of Doxing
* Drop Google
* Grok Virtual Private Networks
* Set Up Command Line ProtonVPN
* Set Up KeePassXC

### Learning to Learn

* Grok [Learning to Learn](/what/)
* Grok Dogma is Death
* Always Be Learning 

### Week One Wrap Up

* Review Everything from Week
* Plan Learning Projects
* Have a Great Weekend

:::todo

------------------------------------------------------------------------
Learning Project Ideas
------------------------------------------------------------------------
Describe how *your home network* works to a non-technical friend or
family member. Or just write down you explanation.

Describe how the *Internet* works to a non-technical friend or family
member. Or just write down your explanation.

Create a [PicoCTF](https://picoctf.com) account and connect to it 
with ssh through a VPN.

Connect to [OverTheWire](https://overthewire.org) with ssh over a VPN.

Practice copying files between PicoCTF and/or OverTheWire with `scp`.

Make a plan and set a goal to get off Google completely.

Setup your own KeePassXC database with auto-typing activated.

Add your *private* SSH keys to KeePassXC and activate SSH Agent

Read through the lengthy old Lynx configuration files.

Install and sample other text-only Web browsers compared to Lynx.

Ponder what cognitive biases you might have and how to protect against

Dig deeply into what it means to be an [autodidact](/what/autodidact/).

Set some specific skill goals to reduce the number of questions you have
to ask that you can research and answer on your own.

Find an opportunity to contradict or disagree with someone without 
being triggered or triggering them.

Practice balancing no-fear, strong disagreement with respect and concern
for the person attacking the information, concept, or tool and not the
person.
------------------------------------------------------------------------

:::

## Week Two: Grokking, Installing, and Running Linux{#wk02}

### Day 6

[📺]{.noinvert} [Linux Beginner Boost - Day 6 (May 11, 2020)](https://youtu.be/v9z-FqNPX90)

#### What is Linux?

* Grok Why and Where Linux Exists 
* Meet Richard Stallman, Creator of GNU
* Meet Linus Torvaldz, Creator of Linux
* Grok Why GNU/Linux is Disrespectful
* Grok Why Mac is Not Linux
* Grok Why Windows is *Becoming* Linux

#### Getting Linux 

* Review Linux Terminal from Week One
* Grok Cloud vs Virtual Machine vs Hardware

#### Licensing and Legal Considerations

* Don't Taint Yourself
* Know What You're Signing
* Grok Free Software Licenses
* Grok Open Source Licenses
* Pick Apache V2 License for Permissive
* Pick GPLv2 License for Copyleft
* Know Difference Between GPLv2 and GPLv3
* Avoid GPLv3 in General
* Grok GPLv3 Implication for Embedded
* Grok Creative Commons Licenses
* Grok Open Core Business Model

#### Certification

* Know Why and When to Certify (It's About Trust)
* Grok Proof is in the Projects, Not Paper
* Discuss LPI Linux Certifications
* Leverage LPI Outlines for Learning
* Discuss Alternatives to Certification
* Avoid CompTIA for Breech of Ethics
* Watch Out for Cert Scammers

#### Linux Distributions

* Know the Main [Distros](/tools/linux/distros/)
* Grok Why Linux from Scratch is Not a Distro
* Pick the Right Distros
* Debate Distros

### Day 7

[📺]{.noinvert} [Linux Beginner Boost - Day 7 (May 12, 2020)](https://youtu.be/2TYTrsK_ry0)

#### Know the Linux Hosting Providers

* Meet Amazon Web Services
* Meet Google Compute Engine
* Meet Microsoft Azure
* Meet Digital Ocean
* Meet RackSpace
* Meet Linode

#### What Can You Do?

* Host an Agar.io Clone
* Host a Doom server
* Host Your Own Websockets Game
* Host Your Own Database with GraphQL API
* Grok Why Hosting Web Site is Bad Idea
* Grok Appeal of Hosting APIs

#### Create a Digital Ocean Droplet

* Set Up SSH Keys
* Create a Digital Ocean Account
* Create the Droplet
* Login Remotely

#### Set Up Paper Minecraft Server

* Download Server with Curl
* Grok Minecraft Licensing
* Start Up in Screen or TMUX
* Give Op
* Connect and Try It Out

### Day 8

[📺]{.noinvert} [Linux Beginner Boost - Day 8 (May 13, 2020)](https://youtu.be/yraQ7kILVkE)

#### Grok [Virtualization](/what/virt/)

* A Machine Within a Machine
* IBM Invented It
* VMWare Popularized It and Won the Enterprise
* VirtualBox Won the Open Market

#### Grok [Containerization](/what/contain/)

* Houses and Apartments Analogy
* Docker to Create Container
* Kubernetes to Manager the Containers

#### Run a Linux Docker Container

* Download and Install Docker
* Authorize Your User
* Create an Account on <https://hub.docker.com>
* Run Hello World Container
* Run Ubuntu Server (or Other) Container with Bash
* Exit and Restart Container in Daemon Mode
* Reconnect to Named Container
* Commit Changes to Container
* Stop the Container
* Remove the Old Container

#### Create and Run a Linux Virtual Machine

* Download and Install VirtualBox
* Download and Install PopOS (or Other)
* Create a VirtualBox Virtual Machine
* Boot the Virtual Machine with Media Image
* Complete the Installation and Setup

### Day 9

[📺]{.noinvert} [Linux Beginner Boost - Day 9 (May 14, 2020)](https://youtu.be/LfD6YFsYkCg)

#### Find a Computer

* Linux Runs on Anything
* Look Around (No Seriously)
* Get a Raspberry Pi (Not Arduino)
* Be Sure You Can Break It!

#### Create a Bootable USB Drive

* Dangers of DD Disk Destroyer
* Why Not Rufus or Other Tools 
* Download Balena Etcher, Grok Why
* Download a Linux Install Image
* Grok Live Booting
* Get a USB Thumb Drive
* Flash Image to Thumb Drive

#### Understand Boot Process

* Grok BIOS
* Grok Boot Process
* Grok Boot Order
* Grok Disk Partitions
* Grok Boot Loaders
* Grok UEFI
* Grok Grub
* Grok Init Levels
* Grok Danger of Powering Off
* Research Your Computer Type
* Identify Setup Key Sequence
* Change Your Boot Order to USB First

#### Know If and When to Dual Boot

* Grok Pros and Cons of Dual Booting
* Consider Mint or Easy Dual Boot

#### Practice Live Booting

* Boot from a USB
* Live Boot to Install
* Live Boot to Recover
* Live Boot to Pwn (SecOps)
* Create Persistent Live Boot

#### Make It Safe

* Evaluate Your Security Needs
* Pick a Secure Distro
* Update and Upgrade Immediately
* Consider How and Where Will Be Used
* Consider Disk Encryption
* Get a Wifi Dongle Just In Case
* Consider Ethernet Over Wifi
* Create a User with Sudo
* Check for Open Listeners
* Consider Backup Strategies

### Day 10

[📺]{.noinvert} [Linux Beginner Boost - Day 10 (May 15, 2020)](https://youtu.be/X0ArQfix31U)

#### Use Software Package Managers

* Grok No [App Stores](/advice/dont/appstores/)
* Grok [What About PWA](/what/web/pwa/)
* Grok How Packages Define Distros
* Know *Your* Package Manager
* Use `dpkg` and `apt`
* Use `apt update`
* Use `apt upgrade`
* Grok PPA
* Add a PPA

#### Understand Professional Linux Occupations

* Research Software Engineer
* Research DevOps Engineer
* Research Site/Service Reliability Engineer
* Prove Your Skills

#### Review the Week

* Read Over Material
* Log About the Week
* Set Goals for the Weekend
* Discuss Learning Project Ideas

## Week Three: The Linux Command Line, I-III{#wk03}

### Day 11

*Read and practice everything through page 127*

#### Grok Annotations Approach

* The [W](/what/rwx/w/) in [RWX](/what/rwx/)
* [Books](/reviews/books/) Still Matter
* Build on Work of Others
* Opinions are Good

#### Logistics

* How to view a PDF
* Overview of Week

#### Learning Project Ideas

* <https://OverTheWire.org/wargames/bandit>
* <https://exercism.io>
* <https://typing.com>

#### Part 1: Learning the Shell

* What is the Shell?
* Navigation
* Exploring the System
* Manipulating Files and Directories
* Working with Commands

### Day 12

*Read and practice everything through Page 170*

#### Part 1: Learning the Shell (Continued)

* Redirection
* Seeing the World as the Shell Sees It
* Advanced Keyboard Tricks (`set -o vi`)
* Permissions 
    * Inodes and types
* Processes

### Day 13

*Read and practice everything through Page 161*

#### Part 2: Configuration and the Environment

* The Bash Environment
* (Realized book treats topics is bad order).

### Day 14

* Using the Best of Vi-centric Vim

### Day 15

* Set Up and Configure Vi-centric Vim `vimrc`

## Week Four: The Linux Command Line, IV{#wk04}

*Note that this week will cover the content of Part IV but not in the same order as the book necessarily.*

### Day 16

* TMUX Screen-Centric Configuration
* Bash in an Interpreted Language
* Procedural Programming and Top-Down Design
* Creating Your First Shell Script
    * Creating the File
    * Making the File Into a Script
    * Using Dot Slash for Local Path
    * Adding Scripts Directory to PATH
* Printing Output to the Terminal
* Declaring and Using Variables
    * Using `declare` for Scope and Type
    * Using Curly Brackets to Disambiguate
    * Exporting Variables to Subshells
* Modern ANSI Escapes for RGB Color
    * Using Single Quote Variables
    * Clearing the Screen Efficiently
    * Other Terminal Magic
* Substituting Commands as Variables
* Substituting Integer Math as Variables
    * Beware of Floating Point Arithmetic
        * Use `bc`

### Day 17

* Assigning Long Variable Values
    * Using Quotes Across Multiple Lines
    * Using Here Documents for Assignment
        * Do *Not* Use Initial Tabs
* Working with Parameters and Arguments
* Creating First Subroutine
* When in Doubt, Quote it Out
* Remember *Everything* is a String

### Day 18

* Grok [Mneumonic Mini-Projects](/what/cha/)
* MMP: `greet`
    * Printing Output to the Command Line
    * Reading Interactive User Input
    * Using RGB ANSI Color Escapes
    * Factoring Colors Escapes into Color Library
    * Pros and Cons of Sourcing Files
* MMP: `nyan`
    * Repeating Something Forever with `while true`
    * Clearing the Screen
    * Getting Random Integer with `RANDOM`
    * Pausing with `sleep`

### Day 19

* Grok Variable Scope and Style
    * Learn `declare`
* Grok Test Driven Development
* Grok Practical Code Organization
    * Command or Function?
* MMP: `waffles`
    * Validate Input with Conditions
    * Precise Validation with Bash Regular Expressions
    * Nest If Conditional Statements
    * Factor Repetitive Steps into Subroutines

### Day 20

* MMP: `badgers`
* MMP: `eightball`
    * Put Responses into An Array
* Grok Associative Arrays
* Grok References
* Grok Special Variables
* Grok POSIX If Condition Syntax
    * Learn the `test` Function

