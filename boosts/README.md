---
Title: Beginner Boosts
Subtitle: Get Started Right
Type: Aggregate
Nodes:
- learn
---

Planning Conclusions

* Boosts will have a book/PKA.
* Boosts will have video series that can double as audio podcast.
* Boosts will prioritize verbal explanations.
* Boosts will build on challenge-based learning (filling the gaps).
* Balance boosts with advanced projects work. 

 Category     Boost                                        
------------ -----------------------------------------------
 Personal      Becoming a Prescient Tech Professional (PTP)
 Workspace     Linux Terminal Proficiency
 Back-end      Programming in Go as a First Language
 Front-end     Web Development Fundamentals


