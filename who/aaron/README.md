---
Title: Aaron Swartz
Subtitle: Still Inspiring Generations to Come
Query: true
---

![](./aaron.png)

* Reddit co-founder
* Creative Commons co-creator at 15

Just watch [the movie](https://youtu.be/9vz06QO3UkQ) to understand him. No seriously, go watch it right now. Everyone should.

It is so sad that this world has lost Aaron. He would be here championing so many different ways to free knowledge from SEO-driven lock down, paywalls, and search engines that derive 90% of their revenue from personalized advertising. The world is definitely dumber and more greedy without him.

Aaron was one of the incredibly rare human beings driven by a *sincere* desire to make the world better for everyone, not the quickest way to trick venture capitalists out of their money and run.

## See Also

* [The Internet's Own Boy](https://youtu.be/9vz06QO3UkQ)
