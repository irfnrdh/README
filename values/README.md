---
Title: Values
---

Values drive us to do what we do in the way that we do. Listing values is always a dangerous proposition because it implies that these are the *only* values. Still these values significantly influence my [views](/views/) on tech and the world, so it's best you know them. You don't have to necessarily agree with my values, just agree to follow them while here. Underlying all of these values is the most important value of all: trust. Trust binds us together allowing us to rely on one another in the community. Most violations of trust are grounds for immediate dismissal and ban.

1. [Community](#community)
1. [Individuality](#individuality)
1. [Authenticity](#authenticity)
1. [Presence](#presence)
1. [Responsibility](#responsibility)
1. [Respect](#respect)
1. [Diligence](#diligence)
1. [Empowerment](#empowerment)
1. [Pragmatism](#pragmatism)
1. [Happiness](#happiness)
1. [Purpose](#purpose)
1. [Openness](#openness)
1. [Relevance](#relevance)
1. [Substance](#substance)
1. [Progress](#progress)
1. [Literacy](#literacy)
1. [Simplicity](#simplicity)
1. [Balance](#balance)
1. [Fitness](#fitness)
1. [Earth](#earth)

## Community

It's about people, not products. Very little ever gets done sitting alone behind a keyboard. Opportunities for collaboration have never been greater. I'm love technology but am committed to building a real community above all with as many in-person associations as possible.

## Individuality

Members come from diverse ages, backgrounds, creeds, races, gender, and learning styles. One-size-fits-all education never considered the individual. It sought to produce workers rather than empower people.

[Openness](#openness) and individuality imply [accessibility](/accessibility/).

## Authenticity

We seek *real* learning, *real* tools, *real* change, and *real* results. This means being honest. Members never cheat. With authenticity comes trust.

## Presence

Nothing beats learning in person --- *nothing*. Many online learning options exist and fulfill an important need --- as I hope <span class=spy>skilstak.io</span> will for many --- but these learning methods will *never* match learning side-by-side from an experienced mentor. Humans have learned best this way for thousands of years.

::: callout
Though science has a tough time pinning down the physical phenomena associated with the effect of people learning being in the same room we have enough anecdotal evidence to prove to us that there is *definitely* something going on that does not happen when the same people use video, text, voice, or email. We believe this *directly* affects the learning process.
:::

## Responsibility

Learners must willfully seek learning and work for it. Learning cannot be forced. The word *teach* implies doing something *to* learners when really the only thing to be done is providing opportunities to learn, which they must receive.

## Respect

Respect can be something as small as honoring our [policy](/policy/#what-if-i-am-early-or-late) on being early or late. You respect the learning of others and the time of those helping you learn.

Respect has always been scarce in the tech sector making it rather dangerous to be someone with a new idea or differing opinion. It doesn't help that the industry is full of legitimately intelligent and amazing people who consider themselves better than everyone else.

> ✨ This value has been particularly challenging for every member of our amazing community (and therefore gets a longer explanation).

Technology—like science—involves the competition of ideas—and ideas come from people. The best idea left standing after an intellectual brawl wins. Some ideas deserve to be violently and vocally opposed, unnecessary violation of good existing standards, for example. These debates can approach the level of religious zeal and dogmatism.

But what about the person who created or espouses the idea? The more passionate the person the more likely he or she is to offend or be offended by those who disagree with the idea. Things get personal. It is at these times that *the value of the person must not become less than the value of the idea*.

Things become further complicated when hiring a person for their ideas and intellect, which is pretty-much what a job in tech is today. Now you have money involved. Bad ideas cost money. People with bad ideas regularly cost companies money, lots of money. People who cost a company money should be fired, some are, some aren't.

> Steve Jobs famously called such people "bozos" but included those who did not agree with his vision or ideas.

This difficulty and its tension has often been deconstructed with humor. It was once ok to call your friend and peer a "moron" for not agreeing with your idea or well-researched conclusion. Everyone knew you were exaggerating—until they didn't. This type of humor depends on sarcasm, which rarely conveys through text. The age of trolling, Twitter, and poorly written communication—even with all the emojis—has hampered our ability to disagree in this fun but dangerous way. People can rarely tell if you are being serious or not. Those days are over.

> Linus Torvalds apologized in 2018 for his "code of conflict" and culture of offensive remarks—though they were mostly just cheeky—stepping down temporarily from the Linux Foundation.

The only thing left standing is *respect*, respect for disagreement, respect for those who openly oppose your idea, respect for those with new ideas.

How do we navigate this difficult situation?

*Don't start with conclusions.* Make your case. Know your audience. But also *do not fear disrupting what you sincerely have researched and concluded is a very bad idea*. Open debate is in danger currently for fear of offending people. The "millennial snowflakes" as Torvalds calls them are endangering open debate. It is *critical* that open, passionate, respectful debate and disagreement remain *along with* a renewed respect for the *person* who happens to believe that absolutely horrible idea. Never conflate the two. Technology is *not* religion.

## Diligence

No one gets paid just for being smart. You get paid for work. Being smart can affect what *kind* of work but intelligence alone is not enough. This is a constant challenge for particularly gifted individuals who may get perfect test scores without trying or for whom complicated math comes easily. Often they seek employment just for being smart rather than doing something with their intellect. Without a work ethic and internal motivation to produce results that gift will remain largely waisted, unrealized, unfulfilled.

Diligence also implies a sense of urgency about one's life and purpose. Diligent individuals are not only uncomfortable when they are not contributing to society, they actively seek new ways to make a contribution. They relax like everyone else to avoid burn out, but are primarily motivated by the difference they can make in the world and not their current high-score or rank in any video game. They don't seek to escape. They seek to engage, to discover, and to build.

## Empowerment

The main reason anyone should learn tech is for their own personal empowerment. 

> 💬 "Whether you want to make a lot of money or change the world computer programming is an incredibly [empowering](https://youtu.be/nKIu9yen5nc?t=242) skill to learn." (Hadi Partovi, [Code.org](https://code.org))

Sure you can get a job easier, but more importantly, *you* can do more, *you* can create more, *you* can realize your own dreams with less dependency on others. This sublime confidence is not unique to tech, but definitely a major emotional (and financial) payoff.

## Pragmatism

Learn and use things that work enough to accomplish the objective. Focus on the practical application of technology, the *why* of it’s existence and usage as much as the *how*.

## Happiness

Life is full of things we *have* to do. We focus on finding those things we *get* to do. It might sound crazy, but work and learning should be *happy* pursuits.

## Purpose

Purpose is the ultimate motivator. It creates drive. It answers the *why* question. Without it life is rather dull and learning. With it every morning starts with anxious excitement. 

## Openness

Too many learning opportunities are horded behind costly proprietary doors. This must change. We too must open ourselves to sharing what we have learned.

Openness and [individuality](#individuality) imply [accessibility](/accessibility/).

## Relevance

Learning anything that is not current is simply wasted time. We do not chase trends unless we have objectively concluded they are more empowering and enriching than the status quo. Often relevance means learning something that [many employers want](/what-do-employers-want/). Other times it means learning what none of them value.

## Substance

Empty marketing, buzzwords, and spin are easy to identify when one is truly informed, capable, and experienced. The action is where the work is. Don't talk. Do. *Then* show.

*[["Forgive my rudeness. I cannot abide useless people."](/i-cannot-abide-useless-people/)]*

## Progress

Dogma is the enemy of humanity. When something new comes along we must have the courage to give it a chance. No one is ever done learning. Don't stop.

## Literacy

We love words. Words are power. You can feel them powering you up as they enter your mind. Written words are particularly special because they have been composed with some amount of thoughtful consideration—especially in books. Tech occupations *require lots of reading*. You will need to read at least two hours a day—every day—just to remain [relevant](#relevance).

## Simplicity

Just because we *can* doesn't mean we *should*. Unnecessary complexity hurts everyone. Think of those buttons on your car dashboard you still never use. It's harder to edit than to write, to refactor than to code, to simplify than to spew. [Simplicity](#simplicity) meaning not using bigger [words](/intermediate-reading-level/) than are necessary to get the meaning across when creating learning material.

## Balance

> 💬 Chitta Vritti Nirodha. ~Patanjali

Balance means attending the symphony sometimes instead of playing video games. It means going for a walk outside while you think and even compose ideas on a mobile device. It means staying healthy, active, and building relationships with others even when it isn't our first choice. It means being free from rage and full of equanimity. We seek to be free from the fluctuations of the mind in order to be better technologists, better people.

## Fitness

Mental and physical. We go outside. We walk and talk. We paddle and talk. We do yoga. We skate. We play frisbee. We run. Without a fit vessel you can't do much else.

## Earth

We don't waste. We don't do paper. We renovate. We live modestly and consume thoughtfully. We bike commute. We work from home when we can. We support local small businesses when we meet publicly.
