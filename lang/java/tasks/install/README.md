---
Title: Install Default Java JRE/JDK on Linux
Query: true
---

All you really need is the Java Runtime Engine for most things. But might as well install them both in case you ever need to write and/or compile Java code.

```sh
sudo apt update
sudo apt install default-jre default-jdk
```
