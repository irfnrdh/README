---
Title: Go Programming Language
SubTitle: Golang
---

Go is a programming language designed originally to replace Python,
Java, C/C++ at Google by some of the most experienced engineers who have
ever lived, [Rob Pike](https://duck.com/lite?kd=-1&kp=-1&q=Rob Pike),
[Ken Thompson](https://duck.com/lite?kd=-1&kp=-1&q=Ken Thompson), and
[Robert Griesemer](https://duck.com/lite?kd=-1&kp=-1&q=Robert
Griesemer). It was specifically created to handle *most*
high-performance utilitarian software development needs at Google.
Forged in the white-hot fires of enterprise to slay many very real
problems, it's no surprise this light, fast, and solid tool is a popular
and ideal choice for most back-end work. Go should be considered
required learning by any serious technologist because of its efficiency,
ubiquity, versatility, and sustainability which is why it is the most
significant language of the DevOps movement and in 2017 [Forbes called 
it](https://www.forbes.com/sites/laurencebradford/2017/09/22/the-2-highest-paying-programming-languages-you-maybe-never-heard-of) one of
the "two highest paid languages you maybe never have heard
of," three years after TJ Holowaychuk [switched to
Go](https://medium.com/code-adventures/farewell-node-js-4ba9e7f3e52b)
from Node.

## Learning Go

Learning Go is one of the most rewarding activities you can pursue. The
best way to learn it is to start writing code in it, even the simplest
code. You can use the [language challenges](/lang/cha/) to get some
ideas.

However, when it comes to books, courses, and other materials there is
no real solid leader. Perhaps the best approach is to simply read the
documentation created by the Go team itself. Consider reading these in
the order suggested by the Go team:

1. [The Go Programming Language Specification](https://golang.org/ref/spec)
1. [Tour of Go](/lang/go/tour/)
1. [How to Write Go Code](https://golang.org/doc/code.html)
1. [Effective Go](https://golang.org/doc/effective_go.html)

Don't forget to start a [codebook](/what/codebook/) and get busy trying
out the code you are reading about. It's the only way to remember it.

## Videos

Here are some of videos good for beginners:

* [How to Start a Golang Project Right](https://youtu.be/Ot9Em123Fz8)

## Frequently Answered Questions

#### "I hate Go's reserve type syntax!"

Um, that's not a question. Get over it. It's far superior for several [objective
reasons](https://blog.golang.org/declaration-syntax).

*This is also why Rust uses it, by the way.*

## See Also

* <https://golangresources.com>
* [Junmin Lee, Go Tutorials](https://www.youtube.com/channel/UC-JTtk2nEdWPZTUDOPwEYbg)
* [Go Code Review Comments](https://github.com/golang/go/wiki/CodeReviewComments)
* [Ultimate Go](https://github.com/betty200744/ultimate-go)
* [Introduction to Go Programming](https://www.golang-book.com/)
* [The Little Go Book](https://openmymind.net/The-Little-Go-Book)
* [How to Code in
  Go](https://www.digitalocean.com/community/books/how-to-code-in-go-ebook)
* [Head First Go](https://headfirstgo.com)
* [Go Bootcamp](http://www.golangbootcamp.com/)
* [Learning Go](http://www.miek.nl/go)
* [Go for JavaScript Developers](https://github.com/pazams/go-for-javascript-developers)
* [Go in Action](https://www.manning.com/books/go-in-action)
* [Test-Driven Development with Go](https://leanpub.com/golang-tdd)
* [Go Secure Coding Practices](https://checkmarx.gitbooks.io/go-scp/content/)
* [Network Programming with Go](https://www.apress.com/us/book/9781484226919)
* [Go in Practice](https://www.manning.com/books/go-in-practice)
* [A Go Developer's Notebook](https://leanpub.com/GoNotebook/)
* [The Go Programming Language Phrasebook](https://www.informit.com/store/go-programming-language-phrasebook-9780321817143)
* [Black Hat Go](https://nostarch.com/blackhatgo)
* [Concurrency in Go](https://www.oreilly.com/library/view/concurrency-in-go/9781491941294/)
* [API Foundations in Go](https://leanpub.com/api-foundations)
* [Web Apps in Go, the Anti-Textbook](https://github.com/thewhitetulip/web-dev-golang-anti-textbook)
* [Go Web Programming](https://www.manning.com/books/go-web-programming)
* [Learn to Create Web Applications Using Go](https://www.usegolang.com/)
* [12 Factor Applications with Docker and Go](https://leanpub.com/12fa-docker-golang)
* [Build SaaS Apps in Go](https://buildsaasappingo.com)
* [Let's Go](https://lets-go.alexedwards.net/) 
* [Tutorial Edge Course](https://tutorialedge.net/course/golang/)
* [Programming with Google Go Specialization](https://www.coursera.org/specializations/google-golang/)
* <https://go.dev>
* <https://tour.golang.org> *(Despite the horrible JavaScript-only
  site.)*
* [Go Project Layout](https://medium.com/golang-learn/go-project-layout-e5213cdcfaa2)


