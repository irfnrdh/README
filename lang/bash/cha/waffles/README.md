---
Title: 'Bash MMP: Waffles'
Subtitle: Short-Circuit Logic with Butter and Syrup?
Query: true
---

Watch the [waffles video](https://duck.com/lite?kae=t&q=waffles video) and write a one line program that combines several command functions using short-circuit logical. Remember to create prompts for user input and to fully validate each response.

## Skills and Concepts Covered

The `waffles` Bash [MMP](/what/cha/) builds on the learning from previous skills and [concepts](/what/concept/) learned from previous MMPs adding the following:

* [Bash `while` Loops](https://duck.com/lite?kae=t&q=Bash `while` Loops)
    * [Bash `while` Statement](https://duck.com/lite?kae=t&q=Bash `while` Statement)
    * [Bash `continue` Statement](https://duck.com/lite?kae=t&q=Bash `continue` Statement)
    * [Bash `break` Statement](https://duck.com/lite?kae=t&q=Bash `break` Statement)
* [Bash Flow Control](https://duck.com/lite?kae=t&q=Bash Flow Control)
    * [Bash Conditional Statements](https://duck.com/lite?kae=t&q=Bash Conditional Statements)
        * [Bash `if` Statment](https://duck.com/lite?kae=t&q=Bash `if` Statment)
        * [Bash Double Bracket `[[ ]]` Conditions]()
        * [Bash Conditional Operators](https://duck.com/lite?kae=t&q=Bash Conditional Operators)
    * [Short-Circuit Logic](https://duck.com/lite?kae=t&q=Short-Circuit Logic)
* [Bash Special Variables](https://duck.com/lite?kae=t&q=Bash Special Variables)
* TODO
* [Subroutines, Procedures, and Functions](https://duck.com/lite?kae=t&q=Subroutines, Procedures, and Functions)
* [Procedural Programming Paradigm](https://duck.com/lite?kae=t&q=Procedural Programming Paradigm)
* [Functional Programming Paradigm](https://duck.com/lite?kae=t&q=Functional Programming Paradigm)
* [Code Refactoring](https://duck.com/lite?kae=t&q=Code Refactoring)

Also covered but with less emphasis:

* [POSIX Bracket Conditions `[ ]`]()
* [Bash `test` Command](https://duck.com/lite?kae=t&q=Bash `test` Command)

## Walkthrough


## See Also

* [What is a Mneumonic Mini-Project](/what/cha/)
* [Mark's Original POSIX](https://github.com/muchanem/shell-1)
