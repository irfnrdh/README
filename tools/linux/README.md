---
Title: The Linux Operating System
Subtitle: The World's Most Important, Ubiquitous, and Hidden OS
Query: true
---

There's a good chance you have one or more Linux devices within arms' reach of you right now, including:

* All Android devices
* Traffic signals
* Game consoles
* Video devices
* Smart televisions
* Car dashboards
* Security devices
* Refrigerators
* Toasters
* Toys

:::co-btw
Most home Wifi routers and cable-modems actually run [FreeBSD,](https://duck.com/lite?kae=t&q=FreeBSD) another Unix-like derivative and not-so-distant cousin of Linux developed independently at the same time without the creators knowing cuz no Internetz yet.
:::

There's also a good chance the Web pages you have open are being served to you from a computer running Linux.

Linux is on so many of these things because it can be made to run on just about anything and is entirely open and free to modify as many product developers desire. Such is not the case with larger operating systems like Windows and Apple Macintosh. In fact, as of June 2020, Windows now ships *with Linux inside of it*. 

:::co-mad
No it's *not* called [GNU/Linux](/what/gnulinux/), and frankly hasn't
been for most of its lifetime. Stop disrespecting the thousands of
contributors to anything that *isn't* GNU. By the way, [your GNU is
dead](/views/gnuisdead/).
:::
