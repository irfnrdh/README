---
Title: Ed Command Line Editor
Subtitle: World's First Major Text Editor
Query: true
---

*Ed* is the only [*command line*](/what/hci/ui/command/) editor still in major use. It is still alive, well, and used by discerning pentesters because it is on any Unix system made since 1969 and works without a full visual terminal environment such as that required for [Vi](/tools/editors/vi/) to work.

