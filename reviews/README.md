
## Series

* Mr. Robot
* Chuck
* IT Crowd
* Cowboy Bebob
* Battlestar Galactica
* The Handmaid Tales
* The Prisoner
* Twin Peaks
* The Expanse

## Films

### MUST WATCH!

* Contact
* The Matrix

### A

* Army of Darkness
* Idiocracy
* Green Mile
* Logan's Run
* Soylent Green
* Shawshank Redemption
* The Great Hack 
* HyperNormalisation
* In Time
* Wargames
* Tron
* Gattaca
* V for Vendetta
* Ex Machina
* Ghost in the Shell
* wallE
* Children of Men
* Sneakers
* Phase 4 and Andromeda

### B

* Enemy of the State
* Swordfish
* Hackers
* Phenomenon

### Documentary

* Age of Stupid
* Snowden
* The Inernet's Own Boy
