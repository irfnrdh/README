---
Title: Learning Web Design, 5th Edition
Subtitle: Annotations
Query: true
---

*Learning Web Design* has been the standard textbook for learning HTML, CSS, and web images for over 20 years. It has been kept up to date remarkably well but does suffer a bit from age. For example, there is no mention of modern web publishing methods such as [static site generators](/what/ssg/), [JAMStack](/what/web/jamstack/), [lambdas](/what/cloud/faas/), nor [serverless](/what/serverless/) deployments through services like [Netlify](/services/netlify/) and [GitLab](/services/gitlab/). The book is heavy on CSS coverage and extremely light on JavaScript (recommending instead to read the O'Reilly [Learning JavaScript](/reviews/books/eloqjs/) book instead). But, the book's coverage of web images is not to be ignored from a security perspective as well as design.

## Before You Begin

You should create a Git repo directory to contain your notes and work. You can combine your [Markdown](/lang/md/) notes with subdirectories containing the different projects from the book as you work through them.

The book is divided into five parts:

------- --------------------------- ---------------   ----------------------------------------------
 Part    Title                       Importance       Description
------- --------------------------- ---------------   ----------------------------------------------
 I       Getting Started             Meh              These do give a good overview of how the web
                                                      works, but we cover that also during the 
                                                      [Networking and the Internet](/what/net/)
                                                      week. You might also find that these 
                                                      annotations are more useful in preparing to
                                                      do the code in the book since 
                                                      they better match your Linux-specific
                                                      preparations from ealier weeks.
                                                      

 II      HTML for Structure          Essential        Core covererage of HTML.

 III     CSS for Presentation        Essential        Jennifer's core expertise is clearly the
                                                      field of web design with CSS and it really
                                                      shows in this largest of the sections.
                                                      Even though you don't need to memorize
                                                      every detail it contains it is definitely
                                                      worth working through in its entirity ---
                                                      particularly the CSS `FlexBox` and `Grid`
                                                      content.

 IV      JavaScript for Behavior     Skip             Replace with 
                                                      [Eloquent JavaScript](/reviews/books/eloqjs/)
                                                      which also covers HTTP.

 V       Web Images                  Essential        Understanding images is one of the most
                                                      underappreciated areas of web development.
                                                      Images give up your personal location and
                                                      can be used for payloads not to mention
                                                      how important understanding the decision
                                                      to use vector v.s. raster images.
------- --------------------------- ---------------   ----------------------------------------------

Table: Parts of Learning Web Design Book

## Chapter 1
