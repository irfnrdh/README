---
Title: Don't Be a Lazy Learner
Subtitle: Identifying an Anti-Autodidact
Query: true
---

> "This is all just too much talking." (Anonymous Twitch Visitor)

Sometimes understanding how to become one thing means first
understanding what that thing is *not*. Here's a list of characteristics
and behaviors you will find in an *anti*-autodidact, a lazy learner who
would rather make excuses than do the real work to learn on their own. 

**They are bored and confused by words in general.** The more words, the
more they check out, spoken or written. Most have a ridiculously low
vocabulary and don't ever let on how many of the words they don't
understand in any given conversation. 

**They don't have the capacity or motivation to figure things out.**
They say stuff like, "I don't get it" or "It's not working" or "I did
what you said" or "The stupid computer won't..." or "What is happening?"
They ask a lot of questions during movies.

**They want to be in the same space with people who do learn.** They are
uncomfortable working from home. They wander into your cubical at work a
lot. The feel safer in your presence because they don't on their own.
They feel they need you close to learn, even on their own. 

**They love mythology and Marvel movies.** They don't have to think. The
don't even have to imagine. There are fewer words to confuse them. They
just plugin and go for a ride. This is also why they hate reading,
role-play, and sandbox video games.

**They are prone to religion and tutorials.** Religion is just a
tutorial for life. They are the same thing, a recipe for how to do
something step-by-step without having to think about *why* you are doing
something. Just do what someone says and you'll have your app or your
key to Heaven. It's the same appeal. Working out how to make your own
app or discovering modern ethical behaviors are hard, too hard for a
lazy learner. Lazy learners want to be told what to do.

**They ask lazy questions.** "Which distro should I use?" "Which window
manager is best?" "What certificate do I need?" "What college should I
attend?" "Which religion should I join?" "What should I eat?" Again,
they would rather have a recipe than analyze anything for a specific
situation. They want answers, not being told to figure it out
case-by-case.

**They tend to cheater because they don't value learning at all.** They
are more focused on what they can *get* than what they can learn. They
would hate Kant's categorical imperative if they even knew what that
meant.

**They blame others for their lack of learning.** Rather than take
responsibility for their own learning they will blame teachers, schools,
parents, their country, the economy, the COVID virus, and anyone else
they can wrangling into their warped justification for not learning a
damn thing --- because of their *own* laziness. It hurts to much to
realize how much they actually suck at learning.

**They lack self-awareness and avoid introspection.** They would rather
be distracted or chasing their urgency addiction the seeking personal
truth. When you ask them what they want to do with their lives --- or
even the current day --- they struggle to answer. Asking them what they
are good at usually results in long pauses.

**They are often depressed or over-confident.** Alan Watts states that
"one cannot experience pleasure without skill." Many are brimming over
with Dunning-Kruger effect having no idea how little they actually need
to learn but never will because they think they know everything already,
that they "have the best words."

