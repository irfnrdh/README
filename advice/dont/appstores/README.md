---
Title: Don't Use and Promote App Stores
Subtitle: What the F*** Were We Thinking?!
Query: true
---

> There are 2 billion web users versus 50 million iOS users. (Brian Kennish, former Google engineer turned privacy advocate)

*App stores* used to be where you had to get your apps. They were a *massively failed attempt* to monopolize the ecosystem for everything that runs on *any* device --- even desktop and laptop computers. Thank God smart, informed, ethical people have promoted standards-based alternatives such as technologies like [progressive web apps](/what/web/pwa/) instead that have flourished while app stores dwindle and die like shopping malls during the 2020 COVID crisis. 

:::co-dumb
The idea that three or four companies would entirely control the vast majority of all software distribution through a crappy, poorly-vetted, highly biased, 30% extorted "market" is downright idiotic. One day people in the future will look back at how we collectively lost out minds to allow this to happen.
:::

In fact, the only thing keeping app stores alive today is the control companies like Apple and Google have *on the devices themselves.* In an open market their app stores would perish. But they control that as well. 

## Stop Associating Rise of Mobile Usage with App Store Success

It's important to distinguish the objective [rise](https://www.businessofapps.com/data/app-statistics/) in [use of mobile devices](https://www.statista.com/statistics/232790/forecast-of-apple-users-in-the-us/#statisticContainer) --- especially [Android](https://www.statista.com/topics/840/smartphones/) --- from a false conclusion that app stores are *also* increasing in usage and success. They aren't. If anything, [Microsoft's announcement](https://docs.microsoft.com/en-us/microsoft-edge/progressive-web-apps-edgehtml/microsoft-store) that it will include PWAs and the fact that the [opensource](/what/open/source/) Android OS has 80% market share is evidence that the original, monolithic app store approach has been a massive failure.

If you look at the apps that are *actually* [being downloaded](https://www.businessofapps.com/data/app-statistics/) ---  almost all of which are social media apps that are not available anywhere *but* the app stores --- then you see a clearer picture of how app stores are actually in decline for general application usage and distribution. Unfortunately there are no statistics showing the [rapid growth of progressive web apps](https://www.enviance.com/ehs-insider/future-ehs-the-rise-of-progressive-web-apps) and every decision for a app developer to choose to create a PWA over the native app in an app store.

## Don't Confuse Apps with App Stores

Native apps have definite advantages over progressive web apps in many objective ways. But whatever the adoption rate of native apps it has nothing to do with the success of app *stores*. If anything the fact that Android is *destroying* the rate of iOS apps is because Android apps do not require an app store to be loaded.

Further evidence of this fact is the number of people jail-breaking their iPhones to run apps that *they* choose removing the corrupt control Apple attempts to exert over the market.

## Benefits of App Stores

Despite the obvious greed behind most app stores --- especially Apple --- the vetting process *does* protect *most* people from getting malware, but not all. All app stores have allowed malware and adware to be distributed so the process is not entirely free from flaws, but it is better than just downloading any app from the Internet. 

Thankfully Amazon is [putting the heat](https://www.lifewire.com/how-to-get-paid-apps-free-android-4177674) on Google's Play store and a healthy competitive apps market exists in the Android ecosystem. Such is not true for Apple and the result will be Apple's eventual demise (which is already manifesting in their tumbling stock values and die-hard Apple fans loudly jumping ship and going to Linux and Windows).

## Opensource Android Has 80%

"Android took over as a clear market leader as of the fourth quarter of 2010, and has only further increased its lead since. As of the fourth quarter of 2016, more than 80 percent of smartphones sold were running the Android operating system. Apple’s operating system iOS is its main competitor, accounting for about 15 percent of the share." (Statista)

## See Also

*This 2011 resource is to demonstrate that this failure was predicted well in advance and obvious to anyone paying attention to where all of this was going.*

* [Why Mobile Apps Will Soon Be Dead (2011)](https://www.technologyreview.com/2011/05/19/194615/why-mobile-apps-will-soon-be-dead/)
* [Why Progressive Web Apps Will Replace Native Mobile Apps](https://www.forbes.com/sites/forbestechcouncil/2018/03/09/why-progressive-web-apps-will-replace-native-mobile-apps/)
* [Number of iPhone users in the United States from 2012 to 2021](https://www.statista.com/statistics/232790/forecast-of-apple-users-in-the-us)
